package model;

/**
 * @author Regina Mavrina
 */
public enum PaymentMethod {
    BANK_TRANSFER,
    PAY_PALL,
    CASH;
}
